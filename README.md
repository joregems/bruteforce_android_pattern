# bruteforce_android_pattern

Here is the is a brute force program for key of android pattern.
The usage is:
"./brute gesture.key" 
Where gesture.key is the path of the file from /data/system.

The key is written in hexadecimal only need omit every 0
at beginning from the numbers

| 0 | 1 | 2 |
|---|---|---|
| 3 | 4 | 5 |
| 6 | 7 | 8 |

In the sequence how you need enter the password
touching the first number of the password and sliding
your finger for the screen for every number in sequence from the password
you get from the program

Written in perl
